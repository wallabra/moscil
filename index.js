// dependencies
const Speaker = require('speaker');
const stream = require('stream');
const FileWriter = require('wav').FileWriter;

// our exports
const moscil = {};



// ---------------------------------------


// rendered wave
moscil.$wave = function $wave(samples, frequency) {
    let res = {
        samples: samples,
        frequency: frequency
    };
    
    res.play = (speedScale = 1.0) => {
        let frequency = Math.ceil(res.frequency * speedScale);
        let speaker = new Speaker({
            channels: 1,
            bitDepth: 16,
            sampleRate: frequency,
            samplesPerFrame: 4096
        });
        
        let bufStream = new stream.PassThrough();
        bufStream.pipe(speaker);

        res.samples.forEach((s) => {
            let buffer = Buffer.alloc(4);
            buffer.writeUInt16LE(s);
            bufStream.write(buffer);
        });

        bufStream.end();

        return speaker;
    };

    res.export = (filename) => {
        let fw = new FileWriter(filename, {
            samplerate: frequency,
            bitDepth: 16,
            channels: 1
        });

        res.samples.forEach((s) => {
            let buffer = Buffer.alloc(4);
            buffer.writeUInt16LE(s);
            fw.write(buffer);
        });

        fw.end();
    };

    return res;
};

// oscillator (function wave)
moscil.$oscillator = function $oscillator(func) {
    let res = {
        f: func
    };

    res.renderWave = (seconds = 5, sampleRate = 48000, speedScale = 1.0) => {        
        let samps = [];

        for (let x = 0; x < seconds * sampleRate; x++) {
            let y = res.f(x / sampleRate);
            y = Math.round((y / 2 + 0.5) * 65535);

            samps.push(y);
        }

        return moscil.$wave(samps, sampleRate * speedScale);
    };

    res.speed = (scale = 2) => {
        return moscil.$oscillator((x) => res.f(x * scale));
    };

    res.note = (deltaSemitones = 1) => {
        return res.speed(Math.pow(2, deltaSemitones / 12));
    };

    res.play = (seconds = 5, sampleRate = 48000, speedScale = 1.0) => {
        let frequency = Math.ceil(sampleRate * speedScale);
        let speaker = new Speaker({
            channels: 1,
            bitDepth: 16,
            sampleRate: frequency,
            samplesPerFrame: 4096
        });
        
        let bufStream = new stream.PassThrough();
        bufStream.pipe(speaker);

        for (let x = 0; x < seconds * sampleRate; x++) {
            let b = Buffer.alloc(4);

            let y = res.f(x / sampleRate);
            
            y = Math.round((y / 2 + 0.5) * 65535);
        
            b.writeUInt16LE(y);
            bufStream.write(b);
        }

        bufStream.end();
        return speaker;
    };

    res.export = (filename, seconds = 5, sampleRate = 48000, speedScale = 1.0) => {
        let frequency = Math.ceil(sampleRate * speedScale);
        
        let bufStream = new FileWriter(filename, {
            sampleRate: frequency,
            bitDepth: 16,
            channels: 1
        });

        for (let x = 0; x < seconds * sampleRate; x++) {
            let y = res.f(x / sampleRate);
            y = Math.round((y / 2 + 0.5) * 65535);
            
            let b = Buffer.alloc(4);
            b.writeUInt16LE(y);
            bufStream.write(b);
        }

        bufStream._flush();
    };

    res.volume = (volumeScale = 1.0) => {
        return res.filter((y) => y * volumeScale);
    };

    res.map = (mapFunc) => {
        return moscil.$oscillator((x) => mapFunc(res.f(x), x));
    };

    res.filter = (filterFunc) => {
        return moscil.$oscillator((x) => filterFunc(res.f(x), x) * res.f(x));
    };

    res.fade = (duration) => {
        return res.filter((y, x) => y * Math.max(0, (duration - x) / duration));
    };

    return res;
};

// mixing waves
moscil.mixer = (func) => {
    return (wave1, wave2) => {
        return moscil.$oscillator((x) => func(wave1.f(x), wave2.f(x)));
    };
};



// --------- BASIC WAVES -----------------



moscil.waves = {};

// sawtooth
moscil.waves.sawtooth = function(hertz=200, amplitude=1) {
    return moscil.$oscillator((x) => {
        return amplitude * 2 * (x * hertz - Math.floor(1 / 2 + x * hertz));
    });
};

// triangle
moscil.waves.triangle = function(hertz=200, amplitude=1) {
    return moscil.$oscillator((x) => {
        return amplitude * 2 * Math.abs(2 * (x * hertz - Math.floor(1 / 2 + x * hertz))) - 1;
    });
};

// square
moscil.waves.square = function(hertz=200, amplitude=1) {
    return moscil.$oscillator((x) => {
        return amplitude * Math.sign(Math.sin(x * hertz));
    });
};

// sine
moscil.waves.sine = function(hertz=200, amplitude=1) {
    return moscil.$oscillator((x) => {
        return amplitude * Math.sin(x * hertz);
    });
};

// constant
moscil.waves.constant = function(y = 1) {
    return moscil.$oscillator(() => y);
};

moscil.mixers = {
    modulators: {}
};



// --------- WAVE SYNTHESIS --------------



moscil.mixers.adder = moscil.mixer((a, b) => (a + b) / 2);
moscil.mixers.subber = moscil.mixer((a, b) => (a - b) / 2);
moscil.mixers.power = moscil.mixer((a, b) => Math.pow(a, b));
moscil.mixers.modulators.ring = moscil.mixer((a, b) => a * b);
moscil.mixers.modulators.amplitude = moscil.mixer((a, b) => a * (b / 2 + 0.5));

module.exports = moscil;
function _logit(x) {
    return Math.log(x / (1 - x));
}

function $vec(v) {
    let w = {
        data: Array.from(v),
        dims: v.length,
        size: Math.sqrt(v.reduce((a, b) => a + Math.pow(b, 2), 0)),
        sum: v.reduce((a, b) => a + b, 0),


        add: function (b) {
            if (!('dims' in b)) b = $vec(b);

            let mi = Math.min(v.length, b.dims);
            return $vec(new Array(mi).fill(0).map((_, i) => v[i] + b.data[i]));
        },

        subtract: function (b) {
            if (!('dims' in b)) b = $vec(b);

            let mi = Math.min(v.length, b.dims);
            return $vec(new Array(mi).fill(0).map((_, i) => v[i] - b.data[i]));
        },

        multiplyVec: function (b) {
            if (!('dims' in b)) b = $vec(b);

            let mi = Math.min(v.length, b.dims);
            return $vec(new Array(mi).fill(0).map((_, i) => v[i] * b.data[i]));
        },

        divideVec: function (b) {
            if (!('dims' in b)) b = $vec(b);

            let mi = Math.min(v.length, b.dims);
            return $vec(new Array(mi).fill(0).map((_, i) => v[i] / b.data[i]));
        },

        multiplyFac: function (b) {
            return $vec(v.map((a) => a * b));
        },

        divideFac: function (b) {
            return $vec(v.map((a) => a / b));
        },

        combiner: function (combiner) {
            return function (b) {
                if (!('dims' in b)) b = $vec(b);

                let mi = Math.min(v.length, b.dims);
                return $vec(new Array(mi).fill(0).map((_, i) => combiner(v[i], b.data[i])));
            };
        },

        map: function (func) {
            return $vec(v.map(func));
        }
    };

    return w;
}

$vec.is = function (vec) {
    return !!(vec && vec.data && vec.dims && vec.size && vec.combiner && vec.map);
};

$vec.fill = function (opts) {
    if (opts.map != null)
        return $vec(new Array(opts.length).fill(0).map((_, i, a) => opts.map(i, a)));

    else
        return $vec(new Array(opts.length).fill(opts.value != null ? opts.value : 0));
};

$vec.random = function (dims) {
    return $vec(new Array(dims).fill(0).map(() => _logit(0.25 + 0.5 * Math.random())));
};


module.exports = $vec;